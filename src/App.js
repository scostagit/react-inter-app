import React from 'react';
import './App.css';
import { translate } from 'react-i18next';
import i18n from './translators';

function App({ t }) {
	const setEnglish = () => {
		i18n.changeLanguage('en');
	};

	const setJaponese = () => {
		i18n.changeLanguage('jp');
	};

	const setPortuguese = () => {
		i18n.changeLanguage('pt');
	};

	return (
		<div className="preview">
			<h1>{t('title')}</h1>
			<p className="center">{t('content')}</p>

			<button onClick={setEnglish}>English</button>
			<button onClick={setPortuguese}>Portuguese</button>
			<button onClick={setJaponese}>Japonise</button>
		</div>
	);
}

export default translate('common')(App);
